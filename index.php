<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/card.min.css">
  <!-- Lien au dessus ajax pour le  semantic -->
</head>

<body>
<h1>Computer Science Figures</h1>


  <!-- Mes cards -->
  <div class="ui link cards">
    <?php
    // Lire notre fichier CSV et renvoie un tableau contenant les données du CSV.
    $fichier = fopen("cs_figures.csv", "r"); //fopen: Ouvre un fichier ou une URL
    $premiereLigne = true;
    while (($data = fgetcsv($fichier)) !== FALSE) { // fgetcsv: obtient une ligne depuis un pointeur de fichier et l'analyse pour des champs CSV
      if ($premiereLigne){ // si la premiére ligne est vrai alors on ne l'affiche pas car c'est le header
        $premiereLigne= false;
        continue;
      }
      echo "<div class=\"card\">";
      echo "  <div class=\"image\">";
      echo "    <img src=\"$data[5]\">"; // 5 car l'image est le 5eme élement du tableau
      echo "  </div>";
      echo "  <div class=\"content\">";
      echo "    <div class=\"header\">$data[0]</div>";
      echo "    <div class=\"meta\"><a>$data[2]</a></div>";
      echo "    <div class=\"description\">$data[3]</div>";
      echo "  </div>";
      echo "  <div class=\"extra content\">";
      echo "    <span class=\"right floated\">Born in $data[1]</span>";
      echo "  </div>";
      echo "</div>";
    }

    ?>
  </div>

</body>

</html>